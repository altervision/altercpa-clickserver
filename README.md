# AlterVision CPA Platform ClickServer

Модуль ClickServer для AlterVision CPA Platform позволяет значительно ускорить обработку кликов в вашей CPA-сети. Он совместим с платформой AlterCPA версии 3.1 и выше. Стандартная версия данного модуля включена в поставку платформы. Если по каким-то причинам (например, особая версия libc на сервере) комплектный модуль не запускается, вам потребуется собрать его вручную с помощью этих исходников.

### Системные требования

Для сборки вам необходимо установить на своём сервере Go версии 1.9 или выше. В большинстве случаев, подойдёт команда, похожая на эту:

_Для серверов на базе Debian/Ubuntu:_
```bash
$ apt-get install golang
```

_Для серверов на базе RedHat/Fedora/CentOS:_
```bash
$ yum install golang
```
После установки и настройки Go, необходимо установить зависимости - пакет драйвера MySQL для GoLang.

```bash
$ go get -u github.com/go-sql-driver/mysql
```

Как только вы установите Go 1.9 и драйвер MySQL, можете приступать к этапу сборки.

### Сборка

Для сборки просто запустите файл build.sh и немного подождите. Сборка не займёт много времени.

```bash
$ ./build.sh
```

В папке bin появится четыре файла: `click_linix_x86`, `click_linix_x64`, `click_linix_arm` и `click_macos` - которые отвечают за работу сервера кликов на различных платформах.

### Установка

Вам необходимо переместить полученные при сборке бинарные файлы из папки `bin` в папку `core/click` панели управления вашей сети, заменив существующие.

В некоторых случаях, файлы собираются без возможности запуска и остаются лежать мёртвым грузом. Чтобы всё исправно запускалось, добавьте им атрибут запуска:

```bash
chmod a+x click_linix_x86
chmod a+x click_linix_x64
chmod a+x click_linix_arm
chmod a+x click_macos
```

### Активация

Настройка сервера кликов на стороне платформы выходит за пределы данной документации, обратитесь к документации сети: http://www.altercms.ru/help/clickserver.html

## Связь с разработчиком

- **Сайт**: http://www.altercms.ru
- **Email**: me@wr.su
- **Skype**: altervision13
- **Telegram**: @altervision
 