#!/bin/bash
go get -u github.com/go-sql-driver/mysql
GOOS=linux GOARCH=386 go build -ldflags "-s -w" -o bin/click_linux_x86 ./src
chmod a+x ./bin/click_linux_x86
GOOS=linux GOARCH=amd64 go build -ldflags "-s -w" -o bin/click_linux_x64 ./src
chmod a+x ./bin/click_linux_x64
GOOS=linux GOARCH=arm go build -ldflags "-s -w" -o bin/click_linux_arm ./src
chmod a+x ./bin/click_linux_arm
GOOS=darwin GOARCH=amd64 go build -ldflags "-s -w" -o bin/click_macos ./src
chmod a+x ./bin/click_macos