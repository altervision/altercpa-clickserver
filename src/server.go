// AlterCPA ClickServer
// Version: 1.10
// Copyright 2018-2024 Anton AlterVision Reznichenko

package main

import (
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"hash/crc32"
	"io/ioutil"
	"net"
	"os"
	"runtime"
	"strconv"
	"time"
)

// The server processor
func main() {

	// Parameters
	version := flag.Bool("version", false, "Show current version and test the build")
	srvhost := flag.String("host", "", "Server address to bind to")
	srvport := flag.String("port", "1862", "Server port")
	srvkey := flag.String("key", "secret", "Secret server key")
	srvpid := flag.String("pid", "click.pid", "Server PID-file full path")
	srvtxt := flag.String("stat", "cron-click.txt", "Server STAT-file full path")
	dbtype := flag.Bool("dbtype", false, "Database connection type uses sockets (true - socket, false - TCP)")
	dbsock := flag.String("dbsock", "/var/lib/mysql/mysql.sock", "Database socket full path")
	dbhost := flag.String("dbhost", "localhost", "Database host")
	dbport := flag.String("dbport", "3306", "Database port")
	dbuser := flag.String("dbuser", "root", "Database user")
	dbpass := flag.String("dbpass", "pass", "Database password")
	dbbase := flag.String("dbbase", "cpa", "Database name")
	dbpref := flag.String("dbpref", "cpa_", "Database prefix")
	flag.Parse()

	// Check the version flag to run the tests
	if *version {

		// Simply echo the version parameter
		fmt.Println("AlterCPA Click Server v1.10")

	} else {

		// Create the database connecton
		var dbconn string
		if *dbtype {
			dbconn = *dbuser + ":" + *dbpass + "@unix(" + *dbsock + ")/" + *dbbase
		} else {
			dbconn = *dbuser + ":" + *dbpass + "@tcp(" + *dbhost + ":" + *dbport + ")/" + *dbbase
		}
		initDB(dbconn, *dbpref)
		defer closeDB()

		// Initialize other processes
		ccInit()
		clickInit()
		go serverPID(*srvpid, *srvtxt)
		startServer(*srvhost, *srvport, *srvkey)

	}

}

//
// Working with server
//

// Save server PID
func serverPID(srvpid string, srvtxt string) {
	var m runtime.MemStats
	time.Sleep(5 * time.Second) // Wait for connection check
	for {

		// Save the PID
		pid := strconv.Itoa(os.Getpid())
		ioutil.WriteFile(srvpid, []byte(pid), 0644)

		// Get and save the memory usage
		runtime.ReadMemStats(&m)
		txt := fmt.Sprintf("{\"memory\":%d}", m.Sys)
		ioutil.WriteFile(srvtxt, []byte(txt), 0644)

		// Wait a minute
		time.Sleep(60 * time.Second)

	}
}

// Start the server and listen
func startServer(host string, port string, srvkey string) {

	// Create the listener server
	ln, err := net.Listen("tcp", host+":"+port)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Accept connections
	for {
		c, err := ln.Accept()
		if err != nil {
			//fmt.Println(err)
			continue
		}
		go handleConnection(c, srvkey)
	}

}

// Handle each connection
func handleConnection(c net.Conn, srvkey string) {

	// Decoding message
	var msg map[string]string
	err := json.NewDecoder(c).Decode(&msg)
	if err != nil {
		statusError(c, "bad-request")
		c.Close()
		return
	}

	// For debug only
	//fmt.Println("Received", msg)

	// Check the message key
	if key, ok := msg["key"]; ok {

		// Sipmly compare the keys
		if key != srvkey {
			statusError(c, "bad-key")
			c.Close()
			return
		}

	} else {
		statusError(c, "no-key")
		c.Close()
		return
	}

	// Check the request type to be set
	if _, ok := msg["type"]; !ok {
		statusError(c, "no-type")
		c.Close()
		return
	}

	// Choose between request types
	switch msg["type"] {

	// New click
	case "click":
		clickCreate(c, msg)

	// Click is good
	case "good":
		clickGood(c, msg)

	// Testing request
	case "test":
		statusOK(c)

	// Bad type requested
	default:
		statusError(c, "bad-type")

	}

	c.Close()

}

// Send the OK status
func statusOK(c net.Conn) {

	msg := map[string]string{
		"status": "ok",
	}

	json.NewEncoder(c).Encode(msg)

}

// Send the Error status
func statusError(c net.Conn, error string) {

	msg := map[string]string{
		"status": "error",
		"error":  error,
	}

	json.NewEncoder(c).Encode(msg)

}

// Send the ID status
func statusID(c net.Conn, id string) {

	msg := map[string]string{
		"status": "ok",
		"id":     id,
	}

	json.NewEncoder(c).Encode(msg)

}

// Send the ID and GEO status
func statusGEO(c net.Conn, id string, geo string) {

	msg := map[string]string{
		"status": "ok",
		"id":     id,
		"geo":    geo,
	}

	json.NewEncoder(c).Encode(msg)

}

//
// Working with database
//

// Database object and queries
var db *sql.DB
var dbCreateClick *sql.Stmt
var dbGoodClick *sql.Stmt
var dbGetUser *sql.Stmt
var dbExtUser *sql.Stmt
var dbSetUser *sql.Stmt
var dbGetGEO *sql.Stmt
var dbGetGEO6 *sql.Stmt
var dbSetGEO *sql.Stmt

// Database initialization
func initDB(conn string, pref string) {

	var err error

	// Connect to the database
	db, err = sql.Open("mysql", conn)
	if err != nil {
		checkDB()
	}

	// Creating new click query
	dbCreateClick, err = db.Prepare("INSERT INTO " + pref + "click ( user_id, offer_id, flow_id, test_id, site_id, site_sib, ext_id, ext_uid, ext_src, ext_uid32, ext_src32, click_ip, click_ip6, click_ip32, click_geo, click_date, click_time, click_hour, click_filter, click_unique, click_space, click_good, click_src, click_mobile, click_os, click_browser, click_uid, click_x32, click_x64, click_xm1, click_xm2, click_xm3, click_xm4, click_xm5, click_price, click_cur, utms, utmc, utmn, utmt, utmm, utmd, utms32, utmc32, utmn32, utmt32, utmm32, utmd32 ) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, UNHEX( ? ), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )")
	if err != nil {
		checkDB()
	}

	// Good click simple query
	dbGoodClick, err = db.Prepare("UPDATE LOW_PRIORITY " + pref + "click SET click_good = click_good + 1 WHERE click_id = ? LIMIT 1")
	if err != nil {
		checkDB()
	}

	// Get the user by flow ID
	dbGetUser, err = db.Prepare("SELECT user_id FROM " + pref + "flow WHERE flow_id = ? LIMIT 1")
	if err != nil {
		checkDB()
	}

	// Get the user by ext ID
	dbExtUser, err = db.Prepare("SELECT user_id FROM " + pref + "user_ext WHERE ext_id = ? LIMIT 1")
	if err != nil {
		checkDB()
	}

	// Set the user by click ID
	dbSetUser, err = db.Prepare("UPDATE " + pref + "click SET user_id = ? WHERE click_id = ? LIMIT 1")
	if err != nil {
		checkDB()
	}

	// Get the country by IPv4 address
	dbGetGEO, err = db.Prepare("SELECT `country` FROM " + pref + "geo_ip WHERE `ip` < ? ORDER BY `ip` DESC LIMIT 1")
	if err != nil {
		checkDB()
	}

	// Get the country by IPv6 address
	dbGetGEO6, err = db.Prepare("SELECT `country` FROM " + pref + "geo_ip6 WHERE `ip6` < UNHEX( ? ) ORDER BY `ip6` DESC LIMIT 1")
	if err != nil {
		checkDB()
	}

	// Set the user by click ID
	dbSetGEO, err = db.Prepare("UPDATE " + pref + "click SET click_geo = ? WHERE click_id = ? LIMIT 1")
	if err != nil {
		checkDB()
	}

}

// Check the DB
func checkDB() {
	err := db.Ping()
	if err != nil {
		panic(err)
	}
}

// Closing all DB connections
func closeDB() {

	dbCreateClick.Close()
	dbGoodClick.Close()
	dbGetUser.Close()
	dbExtUser.Close()
	dbSetUser.Close()
	dbGetGEO.Close()
	dbGetGEO6.Close()
	dbSetGEO.Close()
	db.Close()

}

//
// Working with clicks
//

// Tiny cache
var fw2u map[int64]int64
var ex2u map[int64]int64
var ip2c map[int64]string
var ip6c map[string]string

// Init clickers
func clickInit() {
	fw2u = make(map[int64]int64)
	ex2u = make(map[int64]int64)
	ip2c = make(map[int64]string)
	ip6c = make(map[string]string)
}

// Create new click
func clickCreate(c net.Conn, msg map[string]string) {

	// Prepare valiables for geo and needGEO
	var geo string
	var needGEO bool

	// Offer is the main parameter
	offer := ccInt(msg, "offer")
	if offer == 0 {
		statusError(c, "no-offer")
		return
	}

	// Loading additional parameters
	flow := ccInt(msg, "flow")
	test := ccInt(msg, "test")
	site := ccInt(msg, "site")
	sib := ccInt(msg, "sib")
	exti := ccInt(msg, "exti")
	extu, extu32 := ccUTM(msg, "extu")
	exts, exts32 := ccUTM(msg, "exts")
	filter := ccInt(msg, "filter")
	unique := ccBool(msg, "unique")
	space := ccBool(msg, "space")
	ip := ccInt(msg, "ip")
	ip6, ip32 := ccUTM(msg, "ip6")
	tm := ccTime(msg, "tm")
	src := ccInt(msg, "src")
	mobile := ccInt(msg, "mobile")
	os := ccInt(msg, "os")
	browser := ccInt(msg, "browser")
	utms, utms32 := ccUTM(msg, "us")
	utmc, utmc32 := ccUTM(msg, "uc")
	utmn, utmn32 := ccUTM(msg, "un")
	utmt, utmt32 := ccUTM(msg, "ut")
	utmm, utmm32 := ccUTM(msg, "um")
	utmd, utmd32 := ccUTM(msg, "ud")
	cuid := ccStr(msg, "uid")
	cx32 := ccStr(msg, "x32")
	cx64 := ccStr(msg, "x64")
	cxm1 := ccStr(msg, "xm1")
	cxm2 := ccStr(msg, "xm2")
	cxm3 := ccStr(msg, "xm3")
	cxm4 := ccStr(msg, "xm4")
	cxm5 := ccStr(msg, "xm5")
	price := ccStr(msg, "price")
	cur := ccInt(msg, "cur")

	// Cached parameters
	user, needUser := clickGetUser(flow, exti)
	if ip6 != "" {
		geo = clickGetGEO6(ip6)
		needGEO = false
	} else {
		geo, needGEO = clickGetGEO(ip)
	}

	// Working with the time
	dd := time.Unix(tm, 0)
	hour := dd.Hour()
	date := fmt.Sprintf("%04d%02d%02d", dd.Year(), dd.Month(), dd.Day())

	// Add new click to base
	res, err := dbCreateClick.Exec(user, offer, flow, test, site, sib, exti, extu, exts, extu32, exts32, ip, ip6, ip32, geo, date, tm, hour, filter, unique, space, 0, src, mobile, os, browser, cuid, cx32, cx64, cxm1, cxm2, cxm3, cxm4, cxm5, price, cur, utms, utmc, utmn, utmt, utmm, utmd, utms32, utmc32, utmn32, utmt32, utmm32, utmd32)
	if err != nil {
		checkDB()
		return
	}

	// Get the click ID
	uid, err := res.LastInsertId()
	if err != nil {
		checkDB()
		return
	}

	// Some indian code
	id := int64(uid)
	sid := strconv.Itoa(int(uid))

	// Set the GEO if not cached
	if needGEO {
		go clickSetGEO(id, ip)
	}

	// Set the user if not cached
	if needUser {
		go clickSetUser(id, flow, exti)
	}

	// Send the OK status with ID and GEO
	if needGEO {
		statusID(c, sid)
	} else {
		statusGEO(c, sid, geo)
	}

}

// Get click user by flow
func clickGetUser(flow int64, exti int64) (int64, bool) {

	// Check the way to get user ID
	if flow > 0 {

		// Get user ID from flow
		if user, ok := fw2u[flow]; ok {
			return user, false
		} else {
			return 0, true
		}

	} else if exti > 0 {

		// Get user ID from ext
		if user, ok := ex2u[exti]; ok {
			return user, false
		} else {
			return 0, true
		}

	} else {

		// Neither flow nor ext ID
		return 0, false

	}

}

// Set click user by flow
func clickSetUser(click int64, flow int64, exti int64) {

	// Get the user
	var user int64

	// Check the way to get user
	if flow > 0 {

		// Get the user from flow ID
		err := dbGetUser.QueryRow(flow).Scan(&user)
		if err != nil {
			checkDB()
			user = 0
		}

		// Cache user ID for flow ID
		fw2u[flow] = user

	} else if exti > 0 {

		// Get the user from ext ID
		err := dbExtUser.QueryRow(exti).Scan(&user)
		if err != nil {
			checkDB()
			user = 0
		}

		// Cache user ID for ext ID
		ex2u[exti] = user

	} else {

		// Neither flow nor exti found
		user = 0

	}

	// Set the user for click
	dbSetUser.Exec(user, click)

}

// Get GEO by IPv4
func clickGetGEO(ip int64) (string, bool) {

	ip = ip >> 4
	if geo, ok := ip2c[ip]; ok {
		return geo, false
	} else {
		return "zz", true
	}

}

// Get GEO by IPv6
func clickGetGEO6(ip6 string) string {

	// Try to get GEO from the local cache
	ipl := len(ip6) - 4
	ip := ip6[0:ipl]
	if cachedgeo, ok := ip6c[ip]; ok {
		return cachedgeo
	} else {

		// Get the GEO
		var geo string
		err := dbGetGEO6.QueryRow(ip6).Scan(&geo)
		if err != nil {
			checkDB()
			geo = "zz"
		}

		// Cache and set the GEO
		ip6c[ip] = geo
		return geo

	}

}

// Set click GEO by IP
func clickSetGEO(click int64, ip int64) {

	// Get the GEO
	var geo string
	err := dbGetGEO.QueryRow(ip).Scan(&geo)
	if err != nil {
		checkDB()
		geo = "zz"
	}

	// Cache and set the GEO
	ipn := ip >> 4
	ip2c[ipn] = geo
	dbSetGEO.Exec(geo, click)

}

// Mark click as good
func clickGood(c net.Conn, msg map[string]string) {

	// Parse the ID to int
	uid := ccInt(msg, "id")
	if uid > 0 {

		// Send OK status before the DB request
		statusOK(c)
		go dbGoodClick.Exec(uid)

	} else {

		// ID is not correct
		statusError(c, "bad-id")

	}

}

//
// Strings routines
//

// Inner routines
var cc32 *crc32.Table

// Initialize regex and tables
func ccInit() {
	cc32 = crc32.MakeTable(0xEDB88320)
}

// Simple string to int without errors
func ccInt(msg map[string]string, field string) int64 {
	if in, ok := msg[field]; ok {
		out, err := strconv.ParseInt(in, 0, 64)
		if err == nil {
			return out
		} else {
			return 0
		}
	} else {
		return 0
	}
}

// Zen string to boolean as int of 0/1
func ccBool(msg map[string]string, field string) int64 {
	out := ccInt(msg, field)
	if out == 0 {
		return 0
	} else {
		return 1
	}
}

// Zen string to time
func ccTime(msg map[string]string, field string) int64 {
	out := ccInt(msg, field)
	if out > 0 {
		return out
	} else {
		return time.Now().Unix()
	}
}

// String to string
func ccStr(msg map[string]string, field string) string {
	if in, ok := msg[field]; ok {
		return in
	} else {
		return ""
	}
}

// String to string and crc32
func ccUTM(msg map[string]string, field string) (string, string) {
	if in, ok := msg[field]; ok {
		return in, fmt.Sprintf("%d", crc32.Checksum([]byte(in), cc32))
	} else {
		return "", "0"
	}
}
